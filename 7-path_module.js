const path = require('path');
console.log('path.sep', path.sep)

const filepath = path.join('/content/','subfolder','test.js')
console.log('filepath', filepath) // we always get the normalized path

const base = path.basename(filepath)
console.log('base', base)

const absolute = path.resolve(__dirname,'content','subfoler','test.js')
console.log('absolute', absolute)