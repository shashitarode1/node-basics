// console.log(__dirname)
// console.log(__filename)
// console.log('module', module)
// console.log('require', require)
// console.log('process', process)

// setInterval(()=>{
//     console.log("hello this is shashi")
// },1000)

const os = require('os')
// info about current user
const user = os.userInfo();
console.log('user', user)

// system uptivme in seconds
console.log('os.uptime()', os.uptime())

const currentOs={
    name:os.type(),
    release:os.release(),
    totalMem:os.totalmem(),
    freeMem:os.freemem(),
}
console.log('currentOs', currentOs)