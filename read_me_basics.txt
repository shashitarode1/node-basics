variables : 
	global : no windows,
	__dirname -path to current directory
	__filename,
	require - function to use modules 
	module - info about current module
	process - info about env where the program is beign executed

modules : commonJs evey file is module (by default 
		: module encapsulates code (only share minimum)
		: see r-name.js
		: another way is export as you go : e.g. module.exports.singlePersson = person
										  : e.g. module.exports.item = ["shashi","something"]
										  above two will be available 
		: if we have a function inside of a module that we invoke that code will run if we use require('./path-to-that-file');


fs : two flavours : synchronously(non-blocking--prefered) or asynchronously(blocking)
	:sync will going to take more time : it is just like starting a thread in case of asynchronously readfile
	: async is bit messsy but alternative are promises and async await
	: basically we're going to read from the data base and write to the database